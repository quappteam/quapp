var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var socket_io = require( "socket.io" );
var mongoose = require('mongoose');
var session = require('express-session');
var passport = require('passport');
var MongoStore = require('connect-mongo')(session);
var FacebookStrategy = require('passport-facebook').Strategy;
var passportSocketIo = require("passport.socketio");
var Discussion = require('./models/discussion');
var User = require('./models/user');
var ObjectID = require('mongodb').ObjectID;

//route declarations
var routes = require('./routes/index');
var login = require('./routes/login');
var discussions = require('./routes/discussions');
var create = require('./routes/create');

mongoose.connect('mongodb://localhost/quapp');
//mongoose.connect('mongodb://quappadmin:quapper@ds033457.mongolab.com:33457/quapp');

// Express
var app = express();
app.locals.moment = require('moment'); //moment.js

//Socket.io
var io = socket_io();
app.io = io;


// Passport setup

passport.use(new FacebookStrategy({
    clientID: '818987311522141',
    clientSecret: '2f571a1002bb5d29129097a6fdf34c00',
    callbackURL: "http://localhost:3000/auth/facebook/callback",
    profileFields: ['id', 'displayName', 'emails', 'photos'] 
  },
  function(accessToken, refreshToken, profile, done) {
    console.log(profile.photos[0].value);
    User.findOne({'fbId': profile.id }, function(err, oldUser) {
        if(oldUser){ // als de user bestaat: haal uit database
            done(null,oldUser);
        } else{ // als het een nieuwe user is: maak een nieuwe
            var newUser = new User({
                fbId: profile.id,
                name: profile.displayName,
                //email: profile.emails[0].value,  // werkt niet?
                picture: profile.photos[0].value  // werkt niet?
            }).save(function(err,newUser){
                if(err) throw err;
                done(null, newUser);
            })
        }
     if (err) { return done(err); }
        done(null, profile);
        
    });
  }
));

passport.serializeUser(function(user, done) { // user id opslaan in session
    console.log('serializeUser: ' + user.fbId)
    done(null, user.fbId); //failed to serialize user into session when user logs in for the first time. The user does get added into the database and can login fine after clicking 'Sign In' again
});


passport.deserializeUser(function(id, done) { // adhv user id de user uit de database halen
    User.findOne({'fbId': id},function(err,user){
        if(err) done(err);
        if(user){
            //console.log('user fbId: ' + user.fbId);
            //console.log('user name: ' + user.name);
            //console.log('user picture: ' + user.picture);
            done(null,user);
        }
    });
});

  // MongoStore initialize 

var sessionStore = new MongoStore({ url: 'mongodb://localhost/quapp'}, 
                                  function(err){
                                    console.log(err || 'connect-mongo setup ok');
                                  });




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  key: 'express.sid',
  store: sessionStore,
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.use('/', routes);
app.use('/login', login);
app.use('/discussions', discussions);
app.use('/create', create);

// Passport.socket.io setup
io.use(passportSocketIo.authorize({
  cookieParser: cookieParser,
  key:          'express.sid',
  secret:       'keyboard cat',
  store:        sessionStore,
  success:      onAuthorizeSuccess,
  fail:         onAuthorizeFail,
}));

function onAuthorizeSuccess(data, accept){
  console.log('successful connection to socket.io');
  accept(null, true);
}

function onAuthorizeFail(data, message, error, accept){
  if(error)
    throw new Error(message);
  console.log('failed connection to socket.io:', message);
  accept(null, false);
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

//***********************
//        SOCKET EVENTS
//***********************


io.on( "connection", function( socket ){

    
    var activeDisc = socket.handshake.query.discussion
    var allUsers = io.sockets.adapter.rooms[activeDisc];
    var user = socket.request.user; // username
    
    socket.join(activeDisc); // on socket connect, join discussion
    console.log( user.name + " connected" );  
    
    io.to(activeDisc).emit('user_connected', allUsers, user);
    
    
    
    socket.on('disconnect', function() { // on socket disconnect, leave discussion
		console.log('A user disconnected');
        socket.leave(activeDisc);
        io.to(activeDisc).emit('user_disconnected', allUsers, user);
	});
    
    //
    
    
    
    // create new discussion
    socket.on('create_discussion', function(title, x, y) { 
        Discussion.create({title: title, moderator: {name: socket.request.user.name, id: socket.request.user.id}, latitude: x, longitude: y }, function(err, discussion) {
            io.emit('update_discussions', discussion);
            if (io.sockets.connected[socket.id]) {
                io.sockets.connected[socket.id].emit('discussion_created', discussion);
            }
            //socket.emit('discussion_created', )
        });
    });
    
    // delete discussion
    socket.on('delete_discussion', function(discussion_id) { 
        Discussion.remove({_id: discussion_id}, function(err) {
            console.log("error: " + err);
            io.to(activeDisc).emit('discussion_removed');
            
        });
    });
    
    // change discussion status (locked - open)
    socket.on('change_discussion_status', function(discussion_id) { 
        Discussion.findOne({_id:discussion_id}, function(err, discussion){
            console.log("error: " + err);
            console.log(discussion.locked);

            if(discussion.locked){
                discussion.locked = false;
                discussion.save();
            } else{
                discussion.locked = true;
                discussion.save();
            }
            io.to(activeDisc).emit('discussion_status_changed');
            console.log(io.sockets.adapter.rooms);
            
            
            
        });
    });
    
    // ask new question
    socket.on('add_question', function(title, discId) { 
        //console.log(discId);
        //var id = mongoose.Types(discId);
       // var id = ObjectID.createFromHexString(discId);
        //console.log('parsed id: ' + id);
        Discussion.findOneAndUpdate({_id: discId}, 
                                    {$push: {questions: 
                                                {title: title,
                                                 user: {name: socket.request.user.name,
                                                        id: socket.request.user._id
                                                       }
                                                }
                                            }
                                    }, {upsert: true, 'new': true}, function(err, discussion){ 
          if(err){
              console.log(err); 
          } else{
              var allquestions = discussion.questions;
              io.to(activeDisc).emit('update_questions', allquestions[allquestions.length -1], allquestions.length, user);
          }
        });
    });
    
    
    
    // add new comment -- not the best way, too many queries
    socket.on('add_comment', function(text, discId, questionId) { 
        Discussion.findOne({_id:discId}, function(err, discussion){
            discussion.questions.id(questionId).comments.push({user: {name: socket.request.user.name,
                                                                      id: socket.request.user._id,
                                                                      picture: socket.request.user.picture
                                                                    },
                                                               text: text
                                                              });
            discussion.save(function(err, discussion){
                 if(err){
                      console.log(err); 
                  } else{
                      var allcomments = discussion.questions.id(questionId).comments;
                      var comment = allcomments[allcomments.length-1];
                      io.to(activeDisc).emit('update_comments', comment, questionId );
                  }
            });
        });
    });
    
    
     // delete question
    socket.on('delete_question', function(discId, questionId) { 
        Discussion.findOneAndUpdate({_id:discId}, {$pull: { "questions": {_id: questionId }}}, function(err) {
            console.log("error: " + err);
            io.to(activeDisc).emit('question_removed', questionId);
        });
    });
    
    // delete comment
    socket.on('delete_comment', function(discId, questionId, commentId) { 
        Discussion.findOne({_id:discId}, function(err, discussion) {
             discussion.questions.id(questionId).comments.pull({_id: commentId});
             discussion.save();
            console.log("error: " + err);
            io.to(activeDisc).emit('comment_removed', commentId);
        });
    });
    
    
});

io.on('connect', function(socket) {
    // wanneer er een nieuwe user connecteert > laad alle discussies
	Discussion.find()
		.exec(function(err, discussions) {
		socket.emit('load_discussions', discussions);
		});
    
});


module.exports = app;
