var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
    

gulp.task('minify-css', function() {
  gulp.src('./public/stylesheets/*.css')
    .pipe(minifyCSS())
    .pipe(gulp.dest('./public/stylesheets/'))
});

gulp.task('imagemin', function () {
    return gulp.src('./public/images/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('dist'));
});

/*
gulp.task('watch', function(){
    gulp.watch('./public/stylesheets/*.css', ['minify-css']);
})
*/