var express = require('express');
var router = express.Router();

/* GET create page. */
router.get('/', function(req, res, next) {
  if (req.isAuthenticated()) {
      res.render('create', {user: req.user, title: 'Create discussion', loggedin: true });
  } else{
      res.render('create', {user: req.user, title: 'Create discussion', loggedin: false});
  }
});

module.exports = router;