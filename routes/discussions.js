var express = require('express');
var router = express.Router();
var Discussion = require('../models/discussion')

/* GET users listing. */
router.get('/', function(req, res, next) {
  if (req.isAuthenticated()) {
      res.render('discussions', {user: req.user, title: 'Discussions in your area', loggedin: true });
  } else{
      res.render('discussions', {user: req.user, title: 'Discussions in your area', loggedin: false });
  }
});

module.exports = router;

router.get('/:id', function(req, res) {
  Discussion.findOne({ _id: req.params.id}, function(err, discussion) {
    if (err) {
      return res.send(err);
    }
    
    
  if (req.isAuthenticated()) {
      res.render('discussion', {user: req.user, discussion: discussion, loggedin: true});
  } else{
      res.render('discussion', {user: req.user, discussion: discussion, loggedin: false});
  }
  });
});