var express = require('express');
var passport = require('passport');
var router = express.Router();


// Redirect the user to Facebook for authentication.  When complete,
// Facebook will redirect the user back to the application at
//     /auth/facebook/callback
router.get('/auth/facebook', passport.authenticate('facebook'));

// Facebook will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
router.get('/auth/facebook/callback', 
  passport.authenticate('facebook', {successRedirect: '/', failureRedirect: '/' }),
          function(req,res){
    //res.render("index", {user : req.user, loggedin: true});
    });


/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.isAuthenticated()) {
      res.render('index', {user: req.user, title: 'Quapp', loggedin: true });
  } else{
      res.render('index', {user: req.user, title: 'Quapp', loggedin: false });
  }
});

module.exports = router;

router.get('/logout', function(req, res) {
      req.logout();
      res.redirect('/');
  });