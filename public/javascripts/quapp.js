$(document).ready(function(){
    
    var url = window.location.pathname;
    var discussion_id = url.substring(url.lastIndexOf('/') + 1);
    
    var curLat = 0;
    var curLong = 0;
    var activeDisc = $('#questionlist').attr('data-id')
    var moderatorId = $('#discussion__startedby').data('id');
    
    // Socket connection
	var socket = io.connect('http://localhost:3000', {query: "discussion=" + activeDisc});
    
    // Create new discussion
    $(document).on('click', '#create-form__button', function()
    {
        if($.trim($('#create-form__input').val()) != ''){
            socket.emit('create_discussion',
                        $('#create-form__input').val(),
                        $("#create-form__input").attr('data-latitude'),
                        $("#create-form__input").attr('data-longitude')
                       );
        }
        $('#create-form__input').val('');
        return false;
    });
    
    //Add new question
    $(document).on('click', '#add-question__button', function()
    {
        if($.trim($('#add-question__input').val()) != ''){
            socket.emit('add_question',
                        $('#add-question__input').val(),
                        discussion_id
                       );
            $('#add-question__input').val("");
            
        }
        return false;
    });
    
    
    // Add new comment
    $(document).on('click', '.question__addreply__button', function()
    {
        var commentInput = $(this).prev('.question__addreply__input');
        if($.trim($(commentInput).val()) != ''){
            var question_id= commentInput.parents('.question__replylist').data('id');
            socket.emit('add_comment',
                        commentInput.val(),
                        discussion_id,
                        question_id); 
            commentInput.val("");
        }
        return false;
    });
    
    
    // Remove discussion
    $('.discussion__remove').on('click', function()
    {
        socket.emit('delete_discussion',discussion_id);
    });
    
    // Delete question
    $(document).on('click', '.delete_question', function()
    {
        var question_id = $(this).data('id');
        socket.emit('delete_question',discussion_id, question_id);
    });
    
    // Delete comment
    $(document).on('click', '.delete_comment', function()
    {
        var question_id = $(this).parents('.questionlist__listitem').data('id');
        var comment_id = $(this).data('id');
        socket.emit('delete_comment',discussion_id, question_id, comment_id);
    });
    
    
    // Lock or open a discussion
    $('.discussion__lock').on('click', function()
    {
        socket.emit('change_discussion_status',discussion_id);
    });
    
    //********************************
    //     SOCKET EVENT HANDLING
    //********************************
    
     // New user joined discussion, add to list
    socket.on('user_connected', function(allUsers, user)
    {
        if(allUsers){
            var aantalUsers = Object.keys(allUsers).length;
            $('#users_participating').html(aantalUsers + ' users participating');
        }
        
        if(user){
            var listitem = '<li class="discussion__user" data-id="' + user._id + '">' + 
                            '<img src="' + user.picture + '" class="discussion__user__avatar">' +
                            '<span class="discussion__user__username">' + user.name + '</span>' +
                            '<div class="cf"></div></li>';

            $('#discussion__userlist').append(listitem);
        }
        
    });
    
    socket.on('user_disconnected', function(allUsers, user)
    {
        if(allUsers){
            var aantalUsers = Object.keys(allUsers).length;
            $('#users_participating').html(aantalUsers + ' users participating');
        }
        var user_to_delete = $('.discussion__user[data-id="' + user._id + '"]');
        user_to_delete.remove();
    });
    
    // Discussion was created
    socket.on('discussion_created', function(discussion)
    {   
        $('#create-form__button').html("success!");
         $('#create-form__button').after('<p>Your own discussion has just been created: <a href="/discussions/' + discussion._id + '"></p><div class="cf">'+ discussion.title + '</div>')
        
    });
    
    // Discussion status changed from open to locked or locked to open
    socket.on('discussion_status_changed', function()
    {   
        var lock_btn = $('#discussion__lock');
        
        if(lock_btn.data('status') === 'locked'){
            lock_btn.html('Lock');
            lock_btn.data('status', 'open');
            
            $('.question__addreply').html('<form action=""><input type="text" class="question__addreply__input"/><button type="submit" class="question__addreply__button">Send</button></form>');
            
            $('.addquestion').html('<input id="add-question__input" type="text" class="addquestion__input"/><button id="add-question__button" type="submit" class="addquestion__submit">Ask</button>');
            
            
        } else if(lock_btn.data('status') === 'open'){
            lock_btn.html('Unlock')
            lock_btn.data('status', 'locked');
            
            $('.question__addreply').html('The discussion is locked, you can\'t leave any more comments');
            
            $('.addquestion').html('The discussion is locked, you can\'t ask any more questions');
        }
        
        $('#discussion__title').toggleClass('locked');
            
    });
    
    // Discussion was removed, show error to all users viewing the discussion
    socket.on('discussion_removed', function()
    {   
        $('.container').html('<h3 class="error">The discussion you requested does not exist or has been removed</h3>');
    });
    
    // Question was removed
    socket.on('question_removed', function(questionId)
    {   
        question = $('.questionlist__listitem[data-id="' + questionId + '"]');
        question.children().first().addClass('removed-item')
          .on('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
            question.remove();
            var nrOfQuestions = $('.questionlist__listitem').length;
            $('#nrofquestions').html(nrOfQuestions + ' questions');
        });
        
        
    });
    
    // Comment was removed
    socket.on('comment_removed', function(commentId)
    {   
        var comment =  $('.question__reply[data-id="' + commentId + '"]');

         comment.addClass('removed-item')
          .on('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
              $(this).remove();
           });
    });
    
    
    // New question was added, add the new question to the list
    socket.on('update_questions', function(update, qNr, user)
    {   
        var delete_btn = "";
        var moderatorId = $('#discussion__startedby').data('id');

            
           // var delete_btn = '<a href=# data-id="' + update._id + '" class="delete_question">Delete</a>'

        var listitem =  '<li class="questionlist__listitem" data-id="' + update._id + '">' +
                        '<div class="questionlist__item">' +
                        '<span class="questionlist__item__nr">' + qNr + '</span>' + delete_btn +
                        '<span class="questionlist__item__question">' + update.title + '</span>' +
                        '<span class="questionlist__item__askedby">Asked by ' + update.user.name + '</span>' +
                        '</div>' +
                        '<ul class="question__replylist" data-id="' + update._id + '">';
        
                      
        listitem += '<li class="question__reply error--nocomments">Nothing here yet, be the first to leave a reply!</li>' +
            '<li class="question__addreply">' +
                    '<form>' +
                    '<input type="text" class="question__addreply__input">' +
                    '<button type="submit" class="question__addreply__button">Send</button>' +
                    '</form>' +
                    '</ul></li>';
        
        $('#error--noquestion').remove();
        $('#questionlist').append(listitem);
        $('#nrofquestions').html(qNr + ' questions');
        
    });
    
    // New comment was added, add the new comment to the comments section of a specific question
    socket.on('update_comments', function(update, questionId)
    {   
        var commentlist = $('.question__replylist[data-id="' + questionId + '"]');
        var commentlist_form = commentlist.children().last();
        var text = update.text;
        
        var comText = ValidUrl(text);
                
        
        var listitem =  '<li class="question__reply">' +
                        '<img src="' + update.user.picture + '" alt class="question__reply__avatar">' +
                        '<span class="question__reply__username">' + update.user.name + '</span>' +               
                         comText +
                        '</li>';    
        
        commentlist.children('.error--nocomments').last().remove();
        commentlist_form.before(listitem);
        
    });
    
    
    
    
    // New discussion was added, add the new discussion to the discussion overview
    socket.on('update_discussions', function(update)
    {   
        var listitem =  '<li class="discussionlist__listitem" id="discussionlist__listitem">' +
                        '<a href="/discussions/' + update._id + '" class="discussionlist__item">' + 
                        '<span class="discussionlist__item__question">' + update.title + '</span>' +
                        '<span class="discussionlist__item__startedby">Started by ' + update.moderator.name + '</span>'+
                        '<span class="discussionlist__item__distance">' + dist + ' m</span>'+
                        '<div class="cf"></div>';
        $('#discussionlist').append(listitem);
    });
    
    // Load all discussions
    socket.on('load_discussions', function(discussions) {
        
        if (navigator.geolocation)
        {
            navigator.geolocation.getCurrentPosition(function(position){
                
                curLat = position.coords.latitude;
                curLong = position.coords.longitude;

                $.each(discussions, function(i, discussion){

                    var curPos = new google.maps.LatLng(curLat, curLong);
                    var discPos = new google.maps.LatLng(discussion.latitude,discussion.longitude); 
                    var dist = Math.round(google.maps.geometry.spherical.computeDistanceBetween(curPos, discPos));

                    if(dist < 1000){

                        var listitem =  '<li class="discussionlist__listitem" id="discussionlist__listitem">' +
                                        '<a href="/discussions/' + discussion._id + '" class="discussionlist__item">' + 
                                        '<span class="discussionlist__item__question">' + discussion.title + '</span>' +
                                        '<span class="discussionlist__item__startedby">Started by ' + discussion.moderator.name + '</span>'+
                                        '<span class="discussionlist__item__distance">' + dist + ' m</span>'+
                                        '<div class="cf"></div>';
                        $('#discussionlist').append(listitem);
                    };

                });                                     
            });
        }
        else
        {
            y.innerHTML = "Geolocation is not supported by this browser.";
        }
    });
    
    /* GEO LOCATION */
    var x = $("#create-form__input");
    var y = $("#feedback"); 

    function getLocation()
    {
        if (navigator.geolocation)
        {
            navigator.geolocation.getCurrentPosition(showPosition);
        }
        else
        {
            y.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position)
    {    
        x.attr('data-latitude', position.coords.latitude);
        x.attr('data-longitude', position.coords.longitude);
    }
    
    getLocation();
    
    
    /* IMAGE AND LINK FORMING/VALIDATOR */
    
    function ValidUrl(str)
    {
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
                                 '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
                                 '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
                                 '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
                                 '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
                                 '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        if(!pattern.test(str))
        {
            return '<p class="question__reply__answer">' + str + '</p>';
        }
        else
        {
              if( str.indexOf('.jpg') >= 0 || str.indexOf('.png') >= 0 || str.indexOf('.gif') >= 0){
                  return '<img src="' + str + '" alt="' + str + '" class="question__reply__answer"/>';}
            
              else{
                  if( str.indexOf('http://') >= 0 || str.indexOf('https://') >= 0){
                    return '<p><a href="' + str + '" class="question__reply__answer" target="_blank">' + str + '</a></p>';}
                  else{
                      return '<p><a href="http://' + str + '" class="question__reply__answer" target="_blank">' + str + '</a></p>';}
              }
        }
    }        
    
});