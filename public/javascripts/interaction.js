$( document ).ready(function() {
    $("#burger").on('click', function(){
        $("#menu").toggleClass("expand");
    });
    
    $("#info-icon").on('click', function(){

        $("#discussion__info").toggleClass("expand");
        $("#discussion__userlist").toggleClass("expand");
        $("#discussion__description").toggleClass("expand");
        
    });
    
    $(document).on('click', '.questionlist__item', function(){
        var replylist = $(this).next('ul');
        if(replylist.hasClass("expand")){
            replylist.slideUp(400);
        } else{
            replylist.slideDown(400);
        };
        replylist.toggleClass("expand");
    });
});