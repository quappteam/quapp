var mongoose =  require('mongoose');

var commentSchema = new mongoose.Schema({
    user: { 
        name: { type: String, required: true }, 
        id: { type: String, required: true },
        picture: String
    },
    text: String,
    date: { type: Date, default: Date.now }
    //comments: [commentSchema]
});

var Comment = mongoose.model('Comment', commentSchema);

module.exports = Comment;