var mongoose =  require('mongoose');

var userSchema = new mongoose.Schema({
    
    fbId: Number,
    name: String,
    email: String,
    picture: String
});

var User = mongoose.model('User', userSchema);

module.exports = User;