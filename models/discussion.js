var mongoose =  require('mongoose');
var questionSchema = require('./question').schema;

var discussionSchema = new mongoose.Schema({
    title: String,
    moderator: { name: String, id: String },
    date: { type: Date, default: Date.now },
    locked: { type: Boolean, default: false},
    latitude: Number,
    longitude: Number,
    questions: [questionSchema]
});

var Discussion = mongoose.model('Discussion', discussionSchema);

module.exports = Discussion;