var mongoose =  require('mongoose');
var commentSchema = require('./comment').schema;

var questionSchema = new mongoose.Schema({
    user: { 
        name: { type: String, required: true }, 
        id: { type: String, required: true } 
    },
    title: String,
    comments: [commentSchema]
});

var Question = mongoose.model('Question', questionSchema);

module.exports = Question;